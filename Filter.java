package wikimedia_lab.filter;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * 
 * @author Virginie Galtier
 * 
 *         Reads Wikimedia change events from a Kafka topic, filters out the
 *         ones that concern the Wikipedia project, and publishes for each of
 *         these the locale of the project to a different Kafka topic.
 */
public class Filter {

	/*
	 * List of Kafka bootstrap servers. Example: localhost:9092,another.host:9092
	 * 
	 * @see:
	 * https://jaceklaskowski.gitbooks.io/apache-kafka/content/kafka-properties-
	 * bootstrap-servers.html
	 */
	private String bootstrapServers;
	/*
	 * Name of the source Kafka topic
	 */
	private String wikimediaTopicName;
	/*
	 * Name of the destination Kafka topic
	 */
	private String regionTopicName;

	/**
	 * Creates a filter element (provoking infinite execution).
	 * 
	 * @param arg first argument is a list of Kafka bootstrap servers, second
	 *            argument is the name of the source Kafka topic, third argument is
	 *            the name of the destination Kafka topic
	 */
	public static void main(String[] arg) {
		new Filter(arg[0], arg[1], arg[2]);
	}

	/**
	 * Creates a Kafka consumer and a Kafka producer, the consumer reads a wikimedia
	 * change event from a Kafka topic the locale of the event is extracted the
	 * producer publishes the region (until the filter element is interrupted).
	 * 
	 * @param bootstrapServers   list of Kafka bootstrap servers. Example:
	 *                           localhost:9092,another.host:9092
	 * @param wikimediaTopicName name of the source Kafka topic
	 * @param regionTopicName    name of the destination Kafka topic
	 */
	Filter(String bootstrapServers, String wikimediaTopicName, String regionTopicName) {
		this.bootstrapServers = bootstrapServers;
		this.wikimediaTopicName = wikimediaTopicName;
		this.regionTopicName = regionTopicName;

		KafkaConsumer<Void, String> consumer = new KafkaConsumer<Void, String>(
				configureKafkaConsumer(bootstrapServers));
		// reads from any partition of the topic
		consumer.subscribe(Collections.singletonList(wikimediaTopicName));

		KafkaProducer<Void, String> producer = new KafkaProducer<Void, String>(
				configureKafkaProducer(bootstrapServers));

		try {
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				// reads events
				ConsumerRecords<Void, String> records = consumer.poll(timeout);
				for (ConsumerRecord<Void, String> record : records) {
					// parses Json event
					Gson gson = new Gson();
					JsonObject jsonObject = gson.fromJson(record.value(), JsonObject.class);
					JsonObject jsonObjectMeta = jsonObject.getAsJsonObject("meta");
					String domain = jsonObjectMeta.getAsJsonPrimitive("domain").getAsString();
					// filters
					if (domain.contains("wikipedia.org")) {
						// keeps only locale
						String region = domain.split("\\.")[0];
						// publishes to output topic
						producer.send(new ProducerRecord<Void, String>(regionTopicName, null, region));
					}
				}
			}
		} catch (Exception e) {
			System.out.println("something went wrong... " + e.getMessage());
		} finally {
			consumer.close();
			producer.close();
		}
	}

	/**
	 * Prepares configuration for the Kafka producer <Void, String>
	 * 
	 * @return configuration properties for the Kafka producer
	 */
	private Properties configureKafkaProducer(String bootstrapServers) {
		Properties producerProperties = new Properties();

		producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidSerializer.class.getName());
		producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringSerializer.class.getName());

		return producerProperties;
	}

	/**
	 * Prepares configuration for the Kafka consumer <Void, String> All filters
	 * share the "the_filters" consumer group.
	 * 
	 * @return configuration properties for the Kafka consumer
	 */
	private Properties configureKafkaConsumer(String bootstrapServers) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_filters");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning

		return consumerProperties;
	}
}
